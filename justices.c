/* Supreme Court Justice Lookup
 * by Isaac Barsoum
 * Run ./justices
*/

#include <stdio.h>
#include <string.h>

int main() {
  char justice[500];
  char str[] = "Roberts";
  char str1[] = "Alito";
  char str2[] = "Kavanaugh";
  char str3[] = "Gorsuch";
  char str4[] = "Kagan";
  char str5[] = "Sotomayor";
  char str6[] = "Breyer";
  char str7[] = "Thomas";
  char str8[] = "Barrett";
  char str9[] = "Ginsburg";
  int ret, ret1, ret2, ret3, ret4, ret5, ret6, ret7, ret8, ret9;
  printf( "Enter the last name of a Supreme Court Justice: ");
  scanf("%[^\n]", &justice);
  ret = strcmp (str, justice);
  ret1 = strcmp (str1, justice);
  ret2 = strcmp (str2, justice);
  ret3 = strcmp (str3, justice);
  ret4 = strcmp (str4, justice);
  ret5 = strcmp (str5, justice);
  ret6 = strcmp (str6, justice);
  ret7 = strcmp (str7, justice);
  ret8 = strcmp (str8, justice);
  ret9 = strcmp (str9, justice);
  if (ret == 0) {
    printf("Name: John G. Roberts, Jr. \n");
    printf("Born: January 27, 1955 \n");
    printf("Status: Chief Justice \n");
    printf("Nominated by: George W. Bush \n");
    printf("Martin-Quinn Score (1 most liberal, 9 most conservative): 5 \n");
    printf("Fun fact: presided over the Trump impeachment trial \n");
  } else if (ret1 == 0) {
    printf("Name: Samuel A. Alito \n");
    printf("Born: April 1, 1950 \n");
    printf("Status: Associate Justice \n");
    printf("Nominated by: George W. Bush \n");
    printf("Martin-Quinn Score (1 most liberal, 9 most conservative): 8 \n");
    printf("Fun fact: the 110th Justice, the second Italian American, and the eleventh Roman Catholic to serve on the court \n");
  } 
  else if (ret2 == 0) {
    printf("Name: Brett M. Kavanaugh \n");
    printf("Born: February 12, 1965 \n");
    printf("Status: Associate Justice \n");
    printf("Nominated by: Donald J. Trump \n");
    printf("Martin-Quinn Score (1 most liberal, 9 most conservative): 6 \n");
    printf("Fun fact: confirmed by the Senate by a margin of 50-48 \n");
  } else if (ret3 == 0) {
    printf("Name: Neil M. Gorsuch \n");
    printf("Born: August 29, 1967 \n");
    printf("Status: Associate Justice \n");
    printf("Nominated by: Donald J. Trump \n");
    printf("Martin-Quinn Score (1 most liberal, 9 most conservative): 7 \n");
    printf("Fun fact: the first Supreme Court Justice to serve alongside a justice for whom he once clerked \n");
  } else if (ret4 == 0) {
    printf("Name: Elena Kagan \n");
    printf("Born: April 28, 1960 \n");
    printf("Status: Associate Justice \n");
    printf("Nominated by: Barack H. Obama \n");
    printf("Martin-Quinn Score (1 most liberal, 9 most conservative): 4 \n");
    printf("Fun fact: is considered part of the Court's liberal wing but tends to be one of the more moderate justices of that group \n");
  } else if (ret5 == 0) {
    printf("Name: Sonia M. Sotomayor \n");
    printf("Born: June 25, 1954 \n");
    printf("Status: Associate Justice \n");
    printf("Nominated by: Barack H. Obama \n");
    printf("Martin-Quinn Score (1 most liberal, 9 most conservative): 1 \n");
    printf("Fun fact: has been identified with concern for the rights of defendants, calls for reform of the criminal justice system, and making impassioned dissents on issues of race, gender and ethnic identity \n");
  } else if (ret6 == 0) {
    printf("Name: Stephen G. Breyer \n");
    printf("Born: August 15, 1938 \n");
    printf("Status: Associate Justice \n");
    printf("Nominated by: Bill Clinton \n");
    printf("Martin-Quinn Score (1 most liberal, 9 most conservative): 3 \n");
    printf("Fun fact: confirmed by the Senate by a margin of 87-9 \n");

  } else if (ret7 == 0) {
    printf("Name: Clarence Thomas \n");
    printf("Born: June 23, 1948 \n");
    printf("Status: Associate Justice \n");
    printf("Nominated by: George H. W. Bush \n");
    printf("Martin-Quinn Score (1 most liberal, 9 most conservative): 9 \n");
    printf("Fun fact: the second African-American to serve on the Supreme Court \n");

  } else if (ret8 == 0) {
    printf("Name: Amy C. Barrett \n");
    printf("Born: January 18, 1972 \n");
    printf("Status: Associate Justice \n");
    printf("Nominated by: Donald J. Trump \n");
    printf("Martin-Quinn Score (1 most liberal, 9 most conservative): 7 \n");
    printf("Fun fact: was confirmed in the fastest-ever Senate confirmation process \n");

  } else if (ret9 == 0) {
    printf("Rest in peace, RBG");
  } else {
    printf("Sorry, I do not know that name. \n");
    printf("List of Justices: \n Thomas \n Breyer \n Barrett \n Roberts \n Sotomayor \n Kagan \n Gorsuch \n Kavanaugh \n Alito");
  }
  printf("\n");
  return 0;
}
